package com.htc.sozlukproje.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.htc.sozlukproje.R;

public class PasswordResetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        TextView girisEkraninaDonText= (TextView) findViewById(R.id.girisEkraninaDon_textView);

        girisEkraninaDonText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(PasswordResetActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        Button sifreyiSifirlaButton =(Button)findViewById(R.id.passwordreset_button_sifresifirla);

        sifreyiSifirlaButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(PasswordResetActivity.this, PasswordResetSendActivity.class);
                startActivity(intent);
            }
        });
    }
}
