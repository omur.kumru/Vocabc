package com.htc.sozlukproje.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.htc.sozlukproje.R;

public class HesapOlusturActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hesap_olustur);

        Button hesapOlusturButton =(Button)findViewById(R.id.hesapOlustur_button_hesapOlustur);

        hesapOlusturButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(HesapOlusturActivity.this, HesapOlusturSendActivity.class);
                startActivity(intent);
            }
        });

        TextView girisEkraninaDonText= (TextView) findViewById(R.id.girisEkraninaDon_textView);

        girisEkraninaDonText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(HesapOlusturActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
