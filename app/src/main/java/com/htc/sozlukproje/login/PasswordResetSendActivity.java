package com.htc.sozlukproje.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.htc.sozlukproje.R;

public class PasswordResetSendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset_send);

        TextView girisEkraninaDonText= (TextView) findViewById(R.id.girisEkraninaDon_textView);

        girisEkraninaDonText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(PasswordResetSendActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
