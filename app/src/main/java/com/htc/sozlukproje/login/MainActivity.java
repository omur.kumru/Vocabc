package com.htc.sozlukproje.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.htc.sozlukproje.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView sifremiUnuttumText= (TextView) findViewById(R.id.main_textView_sifremiUnuttum);

        sifremiUnuttumText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(MainActivity.this, PasswordResetActivity.class);
                startActivity(intent);
            }
        });

        Button hesapOlusturButton =(Button)findViewById(R.id.main_button_hesapOlustur);

        hesapOlusturButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(MainActivity.this, HesapOlusturActivity.class);
                startActivity(intent);
            }
        });
    }
}
