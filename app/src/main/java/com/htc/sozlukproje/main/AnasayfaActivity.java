package com.htc.sozlukproje.main;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.htc.sozlukproje.R;

public class AnasayfaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);


        View mCustomView = getLayoutInflater().inflate(R.layout.custom_actionbar_main, null);
        mActionBar.setCustomView(mCustomView);
        Toolbar parent =(Toolbar) mCustomView.getParent();
        parent.setPadding(0,0,0,0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0,0);





        ImageButton filterImageButton= (ImageButton) findViewById(R.id.imageButton_filter);
        ImageButton calendarImageButton= (ImageButton) findViewById(R.id.imageButton_calendar);
        ImageButton plusImageButton= (ImageButton) findViewById(R.id.imageButton_plus);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        filterImageButton.setOnClickListener(mAbListener);
        calendarImageButton.setOnClickListener(mAbListener);
        plusImageButton.setOnClickListener(mAbListener);



    }

    private View.OnClickListener mAbListener = new View.OnClickListener() {
        public void onClick(View v) {
            // do something when the button is clicked
            // Yes we will handle click here but which button clicked??? We don't know

            // So we will make
            switch (v.getId() /*to get clicked view id**/) {
                case R.id.imageButton_filter:
                    Toast.makeText(AnasayfaActivity.this,"adasd",Toast.LENGTH_LONG).show();
                    break;
                case R.id.imageButton_calendar:
                    break;
                case R.id.imageButton_plus:
                    break;
                default:
                    break;
            }}};
    }

